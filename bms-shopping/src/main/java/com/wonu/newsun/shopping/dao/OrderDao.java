package com.wonu.newsun.shopping.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.newsun.domain.entity.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName OrderDao
 * @date 2023/11/22 19:48
 * @description
 */
@Mapper
public interface OrderDao extends BaseMapper<Order> {
}
