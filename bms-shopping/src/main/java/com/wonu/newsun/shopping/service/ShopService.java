package com.wonu.newsun.shopping.service;

import com.rabbitmq.client.Channel;
import com.wonu.newsun.domain.entity.Order;
import com.wonu.newsun.domain.dto.ShoppingCarDto;
import com.wonu.newsun.domain.entity.ShoppingCart;
import com.wonu.newsun.domain.vo.MessageVo;
import org.springframework.amqp.core.Message;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ShopService
 * @date 2023/11/22 16:32
 * @description
 */
public interface ShopService {


    void consumeShopping(Message message, Channel channel, MessageVo<Order> messageVo);


    void createShoppingCar(ShoppingCart shoppingCart);

}
