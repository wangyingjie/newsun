package com.wonu.newsun.shopping.service;

import com.wonu.newsun.domain.entity.Goods;

/**
 * @author nameless
 * @version 1.0
 * @ClassName GoodsService
 * @date 2023/11/22 19:46
 * @description
 */
public interface GoodsService {

    void modifyGoods(Goods goods);
}
