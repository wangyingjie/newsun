package com.wonu.newsun.shopping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ShopApp
 * @date 2023/11/22 16:04
 * @description
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.wonu.newsun")
@EnableTransactionManagement
public class ShopApp {
    public static void main(String[] args) {
        SpringApplication.run(ShopApp.class,args);
    }
}