package com.wonu.newsun.shopping.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.wonu.newsun.domain.entity.Goods;
import com.wonu.newsun.mq.constans.MqConst;
import com.wonu.newsun.shopping.service.CanalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author nameless
 * @version 1.0
 * @ClassName CanalServiceImpl
 * @date 2023/11/23 14:44
 * @description
 */
@Service
@Slf4j
public class CanalServiceImpl implements CanalService {

    @Resource
    private RedisTemplate<String,Object> redisTemplate;


    /**
     * @author nameless
     * @date 2023/12/2 16:49
     * @param message
     * @return void
     * @description 由于canal配置了mysql的binlog日志，即模拟了主从复制，中间使用了mq信息传递消息体
     *      Message message是消息结构体，数据库一旦修改，canal监听到发送消息（更改的表数据）到对应的mq交换机和队列
     *      消费者方法对队列进行监听，获取信息，经过对message对象进行处理，更新到redis中，实现数据库与redis的数据同步
     */
    @RabbitListener(queues = MqConst.CANAL_QUEUE)
    @Override
    public void updateToRedis(Message message) {


        log.debug("数据库变更后，canalQueue获取的消息：{}", message.getMessageProperties());
        //将message中的 消息体字节数组 转换为字符串
        String msg = new String(message.getBody());
        // 字符串转化为json对象 (包含key为data的json数组)
        JSONObject json = new JSONObject(msg);
        JSONArray jsonArray = (JSONArray) json.get("data");
        // 获取json数组第一个元素，即实质的消息json对象 其是以表的字段作为key，
        // 但放入redis中的对象，转成json时key是实体类的属性字段，所以放入redis前需处理
        JSONObject jo = (JSONObject) jsonArray.get(0);

        //json对象转字符串
        String joString = jo.toString();
        //字段字符 转驼峰命名
        String camelCase = StrUtil.toCamelCase(joString);
        //json字符串转成javaBean
        Goods goods = JSONUtil.toBean(camelCase, Goods.class);
        //更新redis缓存
        redisTemplate.opsForValue().set("canalTest",goods);


    }
}