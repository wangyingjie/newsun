package com.wonu.newsun.shopping.service.impl;

import cn.hutool.core.util.IdUtil;
import com.wonu.newsun.domain.entity.Order;
import com.wonu.newsun.domain.utils.OrderIdUtils;
import com.wonu.newsun.mq.constans.MqConst;
import com.wonu.newsun.shopping.dao.OrderDao;
import com.wonu.newsun.shopping.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Objects;

/**
 * @author nameless
 * @version 1.0
 * @ClassName OrderServiceImpl
 * @date 2023/11/22 19:49
 * @description
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private OrderDao orderDao;
   /* @Override
    public void addOrder(Order order) {

        //设置订单号 雪花id
        long snowflakeId = IdUtil.getSnowflakeNextId();
        if(Objects.isNull(order.getNumber())) {
            order.setNumber(snowflakeId);
        }
        order.setStatus(0);
        orderDao.insert(order);
    }*/


    /**
     * @author nameless
     * @date 2023/11/23 16:33
     * @param
     * @return void
     * @description   使用延迟队列方式，则普通队列需要设置发送方式，是一个函数接口
     *   Message postProcessMessage(Message var1)
     */
    @Override
    public void addOrder(Order order) {



        long delayTime = 10000; //延迟时间
        //存入redis , 先查询map生成orderId
        //String orderIdByUser = OrderIdUtils.getOrderIdByUser(order.getUserId());

        //创建雪花id 、订单默认状态
        long snowflakeNextId = IdUtil.getSnowflakeNextId();
        order.setNumber(snowflakeNextId);
        order.setStatus(0);
        orderDao.insert(order);

        //redisTemplate.opsForHash().put("orders", "orderFrom:" + orderIdByUser, order);

        rabbitTemplate.convertAndSend(MqConst.NORMAL_ROUTING_EXCHANGE
        ,MqConst.NORMAL_ROUTING_KEY
        ,order
        , (postProcess) ->{
                    MessageProperties properties = postProcess.getMessageProperties();
                    properties.setExpiration(String.valueOf(delayTime));
                    return postProcess;});
        log.debug("当前发送时间:{}", new Date());
    }

    @RabbitListener(queues = MqConst.DEAD_LETTER_QUEUE)
    @Override
    public void confirm(Order order) {

        log.debug("接收消息的时间:{}, 具体消息：{}", new Date(), order);

        //判断订单是否支付，支付判断库存是否充足，充足则创建订单，否住反馈消息，创建失败



    }
}