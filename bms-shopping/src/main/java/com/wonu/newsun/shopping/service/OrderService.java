package com.wonu.newsun.shopping.service;

import com.wonu.newsun.domain.entity.Order;

/**
 * @author nameless
 * @version 1.0
 * @ClassName OrderService
 * @date 2023/11/22 19:48
 * @description
 */
public interface OrderService {

    void addOrder(Order order);

    void confirm(Order order);
}
