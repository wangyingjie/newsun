package com.wonu.newsun.shopping.controller;

import com.wonu.newsun.common.result.client.PayResp;
import com.wonu.newsun.common.result.http.HttpResp;
import com.wonu.newsun.common.utils.PayUtils;
import com.wonu.newsun.domain.entity.Order;
import com.wonu.newsun.domain.entity.ShoppingCart;
import com.wonu.newsun.shopping.service.OrderService;
import com.wonu.newsun.shopping.service.ShopService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ShopController
 * @date 2023/11/22 16:32
 * @description
 */
@RestController
@RequestMapping("/api/shop")
@Slf4j
public class ShopController {


    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @Resource
    private ShopService shopService;
    @Resource
    private OrderService orderService;


    /**
     * @author nameless
     * @date 2023/11/27 16:56
     * @param shoppingCart
     * @return HttpResp
     * @description  选购物品来创建购物车到redis
     */
    @PostMapping("/createShoppingCart")
    public HttpResp createShoppingCart(@RequestBody ShoppingCart shoppingCart) {

        shopService.createShoppingCar(shoppingCart);
        return HttpResp.success("加入购物车成功");
    }

    @GetMapping("getShoppingCart")
    public HttpResp getShoppingCart(Long userId) {
        ShoppingCart cart = (ShoppingCart) redisTemplate.opsForHash().get("shoppingCart", "sc:" + userId);
        return HttpResp.success(cart);
    }

    @PostMapping("/create")
    public HttpResp createOrder(Order order) {
        orderService.addOrder(order);
        return HttpResp.success("添加订单完成");
    }


    /**
     * @author nameless
     * @date 2023/11/24 15:03
     * @param order
     * @return HttpResp
     * @description 支付订单接口
     */
    @GetMapping("toPay")
    public HttpResp toPay(Order order) {
        Long orderId = order.getOrderId();
        Long userId = order.getUserId();
        BigDecimal totalPrice = order.getTotalPrice();

        //1. 远程调用（为了解决网络波动等可能的阻塞问题 需要使用异步方式）
        String uri = "http://localhost:9033/api/pay/payOrder?totalPrice="+ totalPrice +"&userId=" +userId;
        PayResp payResp = PayUtils.getInstance().getPayResult(uri);

        //2. 返回  ”订单支付中,请等待后续通知“

        // 创建Get请求,现在支付模块实际是支付宝，非项目代码，需要使用HttpClient进行访问
        return null;
    }
}