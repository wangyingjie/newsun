package com.wonu.newsun.shopping.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.newsun.domain.entity.Goods;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName GoodsDao
 * @date 2023/11/22 19:47
 * @description
 */
@Mapper
public interface GoodsDao extends BaseMapper<Goods> {
}
