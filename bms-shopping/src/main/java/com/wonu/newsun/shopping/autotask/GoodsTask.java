package com.wonu.newsun.shopping.autotask;

import com.wonu.newsun.domain.entity.Goods;
import com.wonu.newsun.shopping.dao.GoodsDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author nameless
 * @version 1.0
 * @ClassName GoodsTask
 * @date 2023/11/27 12:13
 * @description   继承CommandLineRunner实现run方法，使得项目启动就执行
 */
@Component
@Slf4j
public class GoodsTask implements CommandLineRunner {

    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @Resource
    private GoodsDao goodsDao;


    /**
     * @author nameless
     * @date 2023/11/27 15:03
     * @param args
     * @return void
     * @description 将产品表数据放在redis的hash中
     */
    @Override
    public void run(String... args) throws Exception {


        log.debug("预加载所有产品信息");
        List<Goods> goodsList = goodsDao.selectList(null);
        Map<String, Goods> goodsMap = null;
        if(goodsList.size() > 0) {
            goodsMap = goodsList.stream().collect(Collectors.toMap(goods ->
                    String.valueOf(goods.getGoodsId()), goods -> goods));
        }
        redisTemplate.opsForHash().putAll("goods", goodsMap);

    }
}