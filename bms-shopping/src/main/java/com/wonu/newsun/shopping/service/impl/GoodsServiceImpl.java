package com.wonu.newsun.shopping.service.impl;

import com.wonu.newsun.domain.entity.Goods;
import com.wonu.newsun.shopping.dao.GoodsDao;
import com.wonu.newsun.shopping.service.GoodsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author nameless
 * @version 1.0
 * @ClassName GoodsServiceImpl
 * @date 2023/11/22 19:47
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Resource
    private GoodsDao goodsDao;


    @Override
    public void modifyGoods(Goods goods) {

        goodsDao.updateById(goods);
    }
}