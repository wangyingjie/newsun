package com.wonu.newsun.shopping.service;

import org.springframework.amqp.core.Message;

/**
 * @author nameless
 * @version 1.0
 * @ClassName CanalServcie
 * @date 2023/11/23 14:43
 * @description
 */
public interface CanalService {

    void updateToRedis(Message message);
}
