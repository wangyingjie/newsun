package com.wonu.newsun.shopping.service.impl;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.rabbitmq.client.Channel;
import com.wonu.newsun.common.utils.WebSocketUtils;
import com.wonu.newsun.domain.dto.ShoppingCarDto;
import com.wonu.newsun.domain.entity.Goods;
import com.wonu.newsun.domain.entity.Order;
import com.wonu.newsun.domain.entity.ShoppingCart;
import com.wonu.newsun.domain.vo.MessageVo;
import com.wonu.newsun.mq.constans.MqConst;
import com.wonu.newsun.shopping.dao.GoodsDao;
import com.wonu.newsun.shopping.dao.OrderDao;
import com.wonu.newsun.shopping.service.GoodsService;
import com.wonu.newsun.shopping.service.OrderService;
import com.wonu.newsun.shopping.service.ShopService;
import lombok.extern.slf4j.Slf4j;
import org.mockito.internal.matchers.Or;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.websocket.Session;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ShopServiceImpl
 * @date 2023/11/22 16:33
 * @description
 */
@Service
@Slf4j
public class ShopServiceImpl implements ShopService {

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private OrderDao orderDao;

    @Resource
    private GoodsDao goodsDao;



    /**
     * @author nameless
     * @date 2023/11/23 20:54
     * @param message
     * @param channel
     * @param messageVo
     * @return void
     * @description 消费者消息确认机制，配合mq.yml文件中配置的手动消费
     *  MessageVo<Order> messageVo 时为了使用webSocket，便于WebSocketUtils获取其内的session，给在线的用户发消息
     */
    @RabbitListener(queues = MqConst.SHOP_A_QUEUE)
    @Override
    public void consumeShopping(Message message, Channel channel, MessageVo<Order> messageVo) {

        log.debug("message属性: {}", message.getMessageProperties());
        log.debug("channel: {}", channel);
        try {
            // 使用channel对象进行手动消费，发生异常在catch中进行解决方案
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
            log.debug("消息队列确认: {},{}",
                    message.getMessageProperties().getConsumerQueue(), "接收到回调方法");
        } catch (IOException e) {
            log.debug("消费失败: {}",e.getMessage());
            try {
                log.debug("出现异常，准备丢弃当前消息: {}",messageVo);
                channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
            } catch (IOException ex) {
                log.debug("丢弃方法出现异常，详细信息: {}",e.getMessage());
            }
        }

        //修改数据库


    }

    /**
     * @author nameless
     * @date 2023/11/27 11:57
     * @param shoppingCart
     * @return void
     * @description 一个一个添加到购物车中（redis中）
     */
    @Override
    public void createShoppingCar(ShoppingCart shoppingCart) {

        Long userId = shoppingCart.getUserId();
        List<Goods> goodsList = shoppingCart.getGoodsList();
        goodsList.forEach(goods -> {
            redisTemplate.opsForHash().put("shoppingCart", "sc:"+ userId,
                    goods);
        });
        log.debug("用户:{},添加到购物车成功:{}", userId,shoppingCart);
    }


}