package com.wonu.newsun.mq.constans;

/**
 * @author nameless
 * @version 1.0
 * @ClassName MqConst
 * @date 2023/11/21 17:30
 * @description
 */
public interface MqConst {

    String USER_FANOUT_EXCHANGE = "fanout_newSun_user_exchange";
    String USER_DB_QUEUE = "newSun_user_db_queue";
    String USER_WEBSOCKET_MESSAGE_QUEUE ="newSun_user_websocketMsg_queue";
    String SHOP_ROUTING_EXCHANGE = "routing_newSun_shopping_exchange";
    String SHOP_A_QUEUE = "newSun_shopping_a_queue";
    String SHOP_B_QUEUE = "newSun_shopping_b_queue";
    String SHOP_A_ROUTING_KEY = "shoppingA";
    String SHOP_B_ROUTING_KEY = "shoppingB";

    String NORMAL_ROUTING_EXCHANGE = "newSun.normal.exchange";
    String DEAD_LETTER_EXCHANGE = "newSun.dlx.exchange";

    String NORMAL_QUEUE = "newSun.normal.queue";
    String DEAD_LETTER_QUEUE = "newSun.dlx.queue";

    String DEAD_LETTER_ROUTING_KEY = "dlx.routingKey";
    String NORMAL_ROUTING_KEY = "newSun.normal.routingKey";
    String CANAL_QUEUE = "canal.topic.queue";

    String DLX_PLUGIN_QUEUE = "dlx.plugin.queue";
    String DLX_PLUGIN_EXCHANGE = "dlx.plugin.exchange";
    String DELAY_PLUGIN_ROUTING_KEY = "dlx.plugin.routingKey";
}