package com.wonu.newsun.mq.config;

import com.wonu.newsun.mq.constans.MqConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RabbitMqConfig
 * @date 2023/11/21 17:20
 * @description  mq配置类
 */
@Configuration
@Slf4j
public class RabbitMqConfig {


    /**
     * @param
     * @return MessageConverter
     * @author nameless
     * @date 2023/11/21 17:22
     * @description 使用jackson将对象转换为json字符串
     * rabbitTemplate.setConfirmCallback(); 参数是一个内部接口，且接口是函数式接口
     * public interface ConfirmCallback {
     * void confirm(@Nullable CorrelationData var1, boolean var2, @Nullable String var3);
     * }
     */
    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        log.debug("==装配rabbitMqTemplate==");
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter());

        //设置开启Mandatory,才能触发回调函数,无论消息推送结果怎么样都强制调用回调函数
        rabbitTemplate.setMandatory(true);
        // 生产者发送消息到交换机时触发的确认回调函数
        rabbitTemplate.setConfirmCallback((correlationData, ask, cause) -> {

            log.debug("是否成功:{}", ask);
            log.debug("失败的原因:{}", cause);
        });

        // 交换机绑定队列时 错误 时触发的确认回调函数
        rabbitTemplate.setReturnsCallback((returnedMessage -> {
            log.debug("发送到队列的状态码:{}", returnedMessage.getReplyCode());
            log.debug("反馈信息:{}", returnedMessage.getReplyText());
        }));
        return rabbitTemplate;
    }

    /**
     * @param
     * @return FanoutExchange
     * @author nameless
     * @date 2023/11/21 17:36
     * @description 创建交换机
     */
    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange(MqConst.USER_FANOUT_EXCHANGE);
    }

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(MqConst.SHOP_ROUTING_EXCHANGE);
    }


    @Bean
    public DirectExchange normalExchange() {
        return new DirectExchange(MqConst.NORMAL_ROUTING_EXCHANGE);
    }

    @Bean
    public DirectExchange deadLetterExchange() {
        return new DirectExchange(MqConst.DEAD_LETTER_EXCHANGE);
    }

    @Bean
    public CustomExchange dlxPluginExchange(){
        return new CustomExchange(
                MqConst.DLX_PLUGIN_EXCHANGE,
                "x-delayed-message",
                true,
                false,
                new HashMap<String,Object>(){{
                    put("x-delayed-type","direct");
                }}

        );
    }


    /*创建队列*/
    @Bean
    public Queue userDBqueue() {
        return new Queue(MqConst.USER_DB_QUEUE);
    }

    @Bean
    public Queue userWsqueue() {
        return new Queue(MqConst.USER_WEBSOCKET_MESSAGE_QUEUE);
    }

    @Bean
    public Queue shopAqueue() {
        return new Queue(MqConst.SHOP_A_QUEUE);
    }

    @Bean
    public Queue shopBqueue() {
        return new Queue(MqConst.SHOP_B_QUEUE);
    }

    @Bean
    public Queue normalQueue() {
        return new Queue(MqConst.NORMAL_QUEUE,
                true,
                false,
                false,
                new HashMap<String, Object>() {{
                    put("x-dead-letter-exchange", MqConst.DEAD_LETTER_EXCHANGE);
                    //这里的x-dead-letter-routing-key 的值，就是死信队列绑定死信交换机时的routingKey，
                    // 即确定普通队列需要传递消息的延迟队列是哪个，可以使用# 表示发送到死信交换机上所有的绑定延迟队列中去
                    put("x-dead-letter-routing-key", MqConst.DEAD_LETTER_ROUTING_KEY);
                }});
    }

    @Bean
    public Queue dlxQueue() {
        return new Queue(MqConst.DEAD_LETTER_QUEUE);
    }


    @Bean
    public Queue dlxPluginQueue(){
        return new Queue(MqConst.DLX_PLUGIN_QUEUE);
    }

    /**
     * @param
     * @return Binding
     * @author nameless
     * @date 2023/11/21 17:39
     * @description 绑定广播队列
     */
    @Bean
    public Binding bindDbQueue() {
        return BindingBuilder.bind(userDBqueue()).to(fanoutExchange());
    }

    @Bean
    public Binding bindWsQueue() {
        return BindingBuilder.bind(userWsqueue()).to(fanoutExchange());
    }

    @Bean
    public Binding bindShopAQueue() {
        return BindingBuilder.bind(shopAqueue()).to(directExchange()).with(MqConst.SHOP_A_ROUTING_KEY);
    }

    @Bean
    public Binding bindShopBQueue() {
        return BindingBuilder.bind(shopBqueue()).to(directExchange()).with(MqConst.SHOP_B_ROUTING_KEY);
    }

    @Bean
    public Binding bindNormalQueue() {
        return BindingBuilder.bind(normalQueue()).to(normalExchange()).with(MqConst.NORMAL_ROUTING_KEY);
    }

    @Bean
    public Binding bindingDlxQueue() {
        return BindingBuilder.bind(dlxQueue()).to(deadLetterExchange()).with(MqConst.DEAD_LETTER_ROUTING_KEY);
    }

    @Bean
    public Binding delayxBinding() {
        return BindingBuilder.bind(dlxPluginQueue())
                .to(dlxPluginExchange())
                .with(MqConst.DELAY_PLUGIN_ROUTING_KEY).noargs();

    }


}