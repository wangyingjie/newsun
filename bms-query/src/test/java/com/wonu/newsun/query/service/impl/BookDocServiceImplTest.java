package com.wonu.newsun.query.service.impl;

import com.wonu.newsun.query.service.BookDocService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BookDocServiceImplTest
 * @date 2023/12/5 16:49
 * @description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class BookDocServiceImplTest {

    @Resource
    private BookDocService bds;

    @Test
    public void loadForDB() {

        bds.loadForDB();
    }
}