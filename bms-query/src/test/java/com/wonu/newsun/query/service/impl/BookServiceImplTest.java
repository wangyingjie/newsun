package com.wonu.newsun.query.service.impl;

import com.wonu.newsun.domain.entity.Book;
import com.wonu.newsun.query.service.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BookServiceImplTest
 * @date 2023/12/5 16:23
 * @description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class BookServiceImplTest {

    @Resource
    private BookService bookService;

    @Test
    public void findAll() {
        List<Book> books = bookService.findAll();
        books.forEach(System.out::println);
    }
}