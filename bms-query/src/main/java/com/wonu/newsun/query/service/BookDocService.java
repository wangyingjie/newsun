package com.wonu.newsun.query.service;

import com.wonu.newsun.domain.doc.BookDoc;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BookDocService
 * @date 2023/12/5 16:39
 * @description
 */
public interface BookDocService {

    void loadForDB();

    List<BookDoc> findAll();
}
