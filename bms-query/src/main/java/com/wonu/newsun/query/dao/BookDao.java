package com.wonu.newsun.query.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.newsun.domain.entity.Book;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BookDao
 * @date 2023/12/5 16:19
 * @description
 */
@Mapper
public interface BookDao extends BaseMapper<Book> {

}
