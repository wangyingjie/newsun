package com.wonu.newsun.query.service.impl;
import java.math.BigDecimal;
import java.util.Date;

import com.wonu.newsun.domain.doc.BookDoc;
import com.wonu.newsun.domain.entity.Book;
import com.wonu.newsun.query.dao.BookDocDao;
import com.wonu.newsun.query.service.BookDocService;
import com.wonu.newsun.query.service.BookService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BookDocServiceImpl
 * @date 2023/12/5 16:39
 * @description
 */
@Service
public class BookDocServiceImpl implements BookDocService {

    @Resource
    private BookService bookService;

    @Resource
    private BookDocDao bookDocDao;

    @Override
    public void loadForDB() {

        List<Book> books = bookService.findAll();
        books.forEach(book->
        {
            BookDoc bookDoc = new BookDoc();
            bookDoc.setId(book.getId());
            bookDoc.setTitle(book.getTitle());
            bookDoc.setAuthor(book.getAuthor());
            bookDoc.setISBN(book.getISBN());
            bookDoc.setPrice(book.getPrice());
     bookDoc.setPublication(book.getPublication());
            bookDoc.setIntroduction(book.getIntroduction());
            bookDoc.setCreateTime(book.getCreateTime());

            // 保存书籍文档
            bookDocDao.save(bookDoc);

        });
        System.out.println("加载进ES完成");

    }

    @Override
    public List<BookDoc> findAll() {
        // 获取所有的书籍文档
        Iterable<BookDoc> iterable = bookDocDao.findAll();
        // 使用流将迭代器转换为List
        return StreamSupport.stream(iterable.spliterator(),false).collect(Collectors.toList());
    }
}