package com.wonu.newsun.query;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author nameless
 * @version 1.0
 * @ClassName QueryApp
 * @date 2023/12/5 16:25
 * @description
 */
@SpringBootApplication
public class QueryApp {
    public static void main(String[] args) {
        SpringApplication.run(QueryApp.class,args);
    }
}