package com.wonu.newsun.query.service.impl;

import com.wonu.newsun.domain.entity.Book;
import com.wonu.newsun.query.dao.BookDao;
import com.wonu.newsun.query.service.BookService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BookServiceImpl
 * @date 2023/12/5 16:21
 * @description
 */
@Service
public class BookServiceImpl implements BookService {

    @Resource
    private BookDao bookDao;
    @Override
    public List<Book> findAll() {
        return bookDao.selectList(null);
    }


    public void thread() {
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(
                4,
                10,
                3000,
                TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<>(100),
                new ThreadPoolExecutor.AbortPolicy()
        );
    }
}