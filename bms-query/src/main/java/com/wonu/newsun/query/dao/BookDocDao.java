package com.wonu.newsun.query.dao;

import com.wonu.newsun.domain.doc.BookDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BookDoc
 * @date 2023/12/5 16:37
 * @description
 */
@Repository
public interface BookDocDao extends ElasticsearchRepository<BookDoc, Long> {
}
