package com.wonu.newsun.query.service;

import com.wonu.newsun.domain.entity.Book;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BookService
 * @date 2023/12/5 16:20
 * @description
 */
public interface BookService {

    List<Book> findAll();
}
