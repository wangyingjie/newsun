package com.wonu.newsun.common.utils;

import lombok.Data;

import javax.websocket.Session;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author nameless
 * @version 1.0
 * @ClassName WebSocketUtils
 * @date 2023/11/21 18:50
 * @description  websocket工具类,采用单例饿汉模式
 */

public class WebSocketUtils {

    private ConcurrentHashMap<String, Session> sessionMap = new ConcurrentHashMap<>();

    private Session lastSession;

    private static WebSocketUtils instance = new WebSocketUtils();

    private WebSocketUtils() {
    }

    /**
     * @author nameless
     * @date 2023/11/21 18:56
     * @param
     * @return WebSocketUtils
     * @description 添加同步锁的获取单例方法
     */
    public static synchronized WebSocketUtils getInstance() {

        return instance;
    }

    public void put(String sessionId, Session session) {
        sessionMap.put(sessionId, session);
    }

    public Session get(String sessionId) {
        return sessionMap.get(sessionId);
    }

    public void remove(String sessionId) {
        sessionMap.remove(sessionId);
    }

    public ConcurrentHashMap<String, Session> getSessionMap() {
        return sessionMap;
    }


}