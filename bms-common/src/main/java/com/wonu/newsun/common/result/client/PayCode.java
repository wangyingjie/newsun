package com.wonu.newsun.common.result.client;

import lombok.Getter;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ClientCode
 * @date 2023/11/24 14:40
 * @description
 */
@Getter
public enum PayCode {
    SUCCESS(200, "支付成功"),
    FAILED(500, "支付失败");
    final int code;
    final String message;

    PayCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
