package com.wonu.newsun.common.result.http;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RespCode
 * @date 2023/11/21 16:55
 * @description  返回类型的枚举
 */
@NoArgsConstructor
@Getter
public enum RespCode {
    SUCCESS(200, "响应成功"),
    FAILED(500,"服务器出现错误");
    private int code;
    private String message;

    RespCode(int code, String message) {
        this.code = code;
        this.message = message;
    }


}
