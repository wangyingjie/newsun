package com.wonu.newsun.common.template;

import javax.websocket.Session;

/**
 * @author nameless
 * @version 1.0
 * @ClassName WebSocketTemplate
 * @date 2023/11/21 18:00
 * @description  ws api 的接口模板
 */
public interface WebSocketTemplate {

    void onOpen(Session session);
    void onClose(Session session);
    void onMessage(Session session, String message);
}
