package com.wonu.newsun.common.exception;

import com.wonu.newsun.common.result.http.HttpResp;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BmsExceptionHander
 * @date 2023/11/28 14:47
 * @description
 */
@RestControllerAdvice
public class BmsExceptionHandler {

    @ExceptionHandler(BmsException.class)
    public HttpResp bmsExceptionHand(BmsException exception) {
        return HttpResp.fail(exception.getCode(),exception.getMessage());
    }
}