package com.wonu.newsun.common.exception;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BmsException
 * @date 2023/11/28 14:49
 * @description
 */
@Data
@NoArgsConstructor
public class BmsException extends RuntimeException{

    private int code;
    private String message;

    public BmsException(int code, String message) {
        this.code = code;
        this.message = message;
    }
}