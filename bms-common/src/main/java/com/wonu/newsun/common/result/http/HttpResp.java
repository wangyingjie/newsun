package com.wonu.newsun.common.result.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName HttpResp
 * @date 2023/11/21 16:52
 * @description   统一返回类型
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HttpResp <T>{

    private int code;
    private String message;
    private T data;


    public static <T> HttpResp<T> success(T data) {
        RespCode success = RespCode.SUCCESS;
        return new HttpResp<>(success.getCode(), success.getMessage(), data);
    }

    public static <T> HttpResp<T> fail() {
        RespCode failed = RespCode.FAILED;
        return new HttpResp<>(failed.getCode(),failed.getMessage(),null);
    }

    public static <T> HttpResp<T> fail(int code, String message) {
        return new HttpResp<>(code,message,null);
    }



}