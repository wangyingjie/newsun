package com.wonu.newsun.common.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import javax.websocket.server.ServerEndpoint;

/**
 * @author nameless
 * @version 1.0
 * @ClassName WebSocketConfig
 * @date 2023/11/21 17:57
 * @description  websocket配置类
 */
@Configuration
@Slf4j
public class WebSocketConfig {

    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        log.debug("==websocket配置==");
        return new ServerEndpointExporter();
    }
}