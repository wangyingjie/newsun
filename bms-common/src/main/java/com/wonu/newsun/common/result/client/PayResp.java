package com.wonu.newsun.common.result.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ClientResp
 * @date 2023/11/24 14:40
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PayResp {

    private int code;
    private String message; //支付消息
    private Long userId;
    private BigDecimal amount;
    private Long orderId;

    public static PayResp success(Long userId, BigDecimal amount, Long orderId) {

        PayCode payCode = PayCode.SUCCESS;

        return new PayResp(payCode.getCode(), payCode.getMessage(), userId, amount, orderId);
    }

    public static PayResp fail(Long userId, BigDecimal amount, Long orderId) {

        PayCode payCode = PayCode.FAILED;

        return new PayResp(payCode.getCode(), payCode.getMessage(), userId, amount, orderId);
    }

}