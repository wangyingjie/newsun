package com.wonu.newsun.common.utils;

import cn.hutool.json.JSONObject;
import com.wonu.newsun.common.result.client.PayResp;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * @author nameless
 * @version 1.0
 * @ClassName HttpClientUtils
 * @date 2023/11/24 14:38
 * @description   httpClient 工具类
 */
@Slf4j
public class PayUtils {

    private static PayUtils instance  = new PayUtils();



    public static PayUtils getInstance() {
        return instance;
    }


    /**
     * @author nameless
     * @date 2023/11/24 14:55
     * @param payUri
     * @return PayResp
     * @description 使用httpClient 请求第三方，并接收第三方特定的结构的返回，PayResp就是模拟第三方支付返回的结构
     */
    public PayResp getPayResult(String payUri) {

        // 1. 通过建造者模式获得Http客户端
        // (可以理解为:你得先有一个浏览器;注意:实际上HttpClient与浏览器是不一样的)
        CloseableHttpClient client = HttpClientBuilder.create().build();

        HttpGet httpGet = new HttpGet(payUri);

        // 响应模型
        CloseableHttpResponse response = null;

        PayResp payResp = new PayResp();

        try {
            // 由客户端执行(发送)Get请求
            response = client.execute(httpGet);
            // 从响应模型中获取响应实体
            HttpEntity responseEntity = response.getEntity();
            log.debug("响应状态为:{}" + response.getStatusLine());
            if (responseEntity != null) {
                String jsonStr = EntityUtils.toString(responseEntity);
                JSONObject jsonObject = new JSONObject(jsonStr);
                payResp = jsonObject.toBean(PayResp.class);
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                // 释放资源
                if (client != null) {
                    client.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        log.debug("返回的结果是：{}", payResp);
        return payResp;

    }
}