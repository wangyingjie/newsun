package com.wonu.newsun.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wonu.newsun.common.utils.WebSocketUtils;
import com.wonu.newsun.domain.entity.User;
import com.wonu.newsun.domain.vo.MessageVo;
import com.wonu.newsun.mq.constans.MqConst;
import com.wonu.newsun.user.dao.UserDao;
import com.wonu.newsun.user.service.UserService;
import com.wonu.newsun.user.websocket.UserWebSocket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.websocket.Session;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserServiceImpl
 * @date 2023/11/21 17:06
 * @description
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;

    @Autowired
    private UserWebSocket webSocket;

    public void setWebSocket(UserWebSocket webSocket) {
        this.webSocket = webSocket;
    }

    @Override
    public List<User> findAll() {
        return userDao.selectList(null);
    }


    /**
     * @author nameless
     * @date 2023/11/21 17:17
     * @param
     * @return void
     * @description 作为消费者监听队列，执行添加操作
     *      弊端：消费者中方法出现异常，不可以统一处理，同时消息会不被消费
     */
    @RabbitListener(queues = MqConst.USER_DB_QUEUE)
    @Override
    public void registry(MessageVo<User> userVo) {
        log.debug("【数据库消费者】接收的消息为：{}", userVo);
        User user = userVo.getData();
        String sessionId = userVo.getSessionId();
        try {
            userDao.insert(user);
            //延迟5秒给客户端发送消息
            Thread.sleep(5000);
            /*ConcurrentHashMap<String, Session> map = WebSocketUtils.getInstance().getSessionMap();
            map.forEach((id,session)->{
                try {
                    session.getBasicRemote().sendText(user.getNikeName() + "注册成功^_^");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });*/
            webSocket.sendInfo(sessionId,user.getNikeName() + "恭喜注册成功^_^");
        }catch (Exception exception) {
            log.debug("注册消费者出现异常：" +exception.getMessage());

        }

    }

    @Override
    public User findUserByUsername(String username) {

        QueryWrapper<User> wrapper = new QueryWrapper<User>().eq(StringUtils.hasText(username), "username", username);
        return userDao.selectOne(wrapper);
    }

    @Override
    public User findUserById(Long userId) {
        User user = userDao.selectById(userId);
        if(Objects.isNull(user)) throw new RuntimeException("id未查询到用户");
        return user;
    }

}