package com.wonu.newsun.user.controller;
import java.math.BigDecimal;
import java.util.Date;

import com.wonu.newsun.common.result.http.HttpResp;
import com.wonu.newsun.domain.dto.RegisterDto;
import com.wonu.newsun.domain.entity.User;
import com.wonu.newsun.domain.vo.MessageVo;
import com.wonu.newsun.mq.constans.MqConst;
import com.wonu.newsun.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserContoller
 * @date 2023/11/21 17:11
 * @description
 */
@Slf4j
@RestController
@RequestMapping("/api/user")
public class UserController{


    @Resource
    private UserService userService;
    @Resource
    private RabbitTemplate rabbitTemplate;

    /**
     * @author nameless
     * @date 2023/11/21 17:47
     * @param registerDto
     * @return HttpResp
     * @description 接收前端请求，进行逻辑校验，随后使用rabbitmq进行广播，两个消费者对象分别监听，一个添加到数据库，一个给前端反馈信息
     */
    @CrossOrigin
    @PostMapping("registry")
    public HttpResp registry(@RequestBody RegisterDto registerDto, HttpServletRequest request) {

        String sessionId = request.getHeader("Ws-token");
        log.debug("注册获取的请求参数：{}", registerDto);
        log.debug("获取的请求头参数：{}", sessionId);
        //1、逻辑校验
        if(Objects.nonNull(userService.findUserByUsername(registerDto.getUsername()))) {
            throw new RuntimeException("用户名已存在");
        }

        User user = new User();
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setNikeName(registerDto.getNikeName());

        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setRoleId(4L);

        //2、将user对象广播到队列中去
        MessageVo<User> messageVo = new MessageVo<>();
        messageVo.setData(user);
        messageVo.setSessionId(sessionId);
        rabbitTemplate.convertAndSend(MqConst.USER_FANOUT_EXCHANGE, "", messageVo);

        return HttpResp.success("注册完成，请等待稍后系统通知");
    }

    @GetMapping("/userId")
    public HttpResp getUser(Long userId) {

        User user = userService.findUserById(userId);

        return HttpResp.success(user);
    }

}