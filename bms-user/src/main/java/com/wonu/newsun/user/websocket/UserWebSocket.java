package com.wonu.newsun.user.websocket;

import com.wonu.newsun.common.template.WebSocketTemplate;
import com.wonu.newsun.common.utils.WebSocketUtils;
import com.wonu.newsun.domain.entity.User;
import com.wonu.newsun.mq.constans.MqConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserWebSocket
 * @date 2023/11/21 17:53
 * @description   UserWebSocket类似与Controller但又不太一样，虽然由@Component注释，但对于每个请求，依然会
 *  生成一个对于的bean ，同时可以一样将整个类当成普通类使用，注入到其他位置并调用方法,
 *  但是注入的对象和连接来时的对象，即HttpSession与webSocket的Session并无关联
 *  不是同一个。。解决方案，前端建立连接时，传递一个userId作为静态Map的key，发送消息时，传递Id去map里面查
 */
@Component
@ServerEndpoint("/ws/user")
@Slf4j
public class UserWebSocket implements WebSocketTemplate {

    /*因为静态存在共享，需要使用线程安全的集合类*/
    private static ConcurrentHashMap<String, Session> sessionMap = new ConcurrentHashMap<>();

    /*连接时就给浏览器一个token*/
    @OnOpen
    @Override
    public void onOpen(Session session)  {
        log.debug("用户{}, 开启连接",session.getId());
        try {

            session.getBasicRemote().sendText("wsToken:" + session.getId());
            WebSocketUtils.getInstance().put(session.getId(), session);
            /*sessionMap.put(session.getId(), session);*/

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        sessionMap.put(session.getId(),session);
    }

    @OnClose
    @Override
    public void onClose(Session session) {
        log.debug("用户{}, 关闭连接",session.getId());
        WebSocketUtils.getInstance().remove(session.getId());

    }

    @OnMessage
    @Override
    public void onMessage(Session session, String message) {
        log.debug("【client】：", message);
    }
    
    /**
     * @author nameless
     * @date 2023/11/21 18:59
     * @param 
     * @return void
     * @description 监听到mq队列中的消息，对所有当前连接的用户进行广播
     */
    /*@RabbitListener(queues = MqConst.USER_WEBSOCKET_MESSAGE_QUEUE)
    public void sendInfo(User user) {
        log.debug("【ws消费者】接收的消息：{}",user);

        //将对象转换为json

        //延迟5秒给客户端发送消息
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        ConcurrentHashMap<String, Session> map = WebSocketUtils.getInstance().getSessionMap();
        map.forEach((id,session)->{
            try {
                session.getBasicRemote().sendText(user.getNikeName() + "注册成功^_^");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }*/

    /**
     * @author nameless
     * @date 2023/11/21 21:24
     * @param message
     * @return void
     * @description 给当前连接用户发送消息
     */
    public void sendInfo(String sessionId, String message) {
        try {
            Session session = WebSocketUtils.getInstance().get(sessionId);
            session.getBasicRemote().sendText(message);
            /*sessionMap.get(sessionId).getBasicRemote().sendText(message);*/
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}