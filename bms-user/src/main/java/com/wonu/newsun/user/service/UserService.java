package com.wonu.newsun.user.service;

import com.wonu.newsun.domain.entity.User;
import com.wonu.newsun.domain.vo.MessageVo;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserService
 * @date 2023/11/21 17:05
 * @description
 */
public interface UserService {

    List<User> findAll();

    void registry(MessageVo<User> userVo);

    User findUserByUsername(String username);

    User findUserById(Long userId);
}
