package com.wonu.newsun.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.newsun.domain.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserDao
 * @date 2023/11/21 17:02
 * @description
 */
@Mapper
public interface UserDao extends BaseMapper<User> {
}
