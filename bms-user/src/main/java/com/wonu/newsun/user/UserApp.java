package com.wonu.newsun.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserApp
 * @date 2023/11/21 17:09
 * @description
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wonu.newsun"})
public class UserApp {
    public static void main(String[] args) {
        SpringApplication.run(UserApp.class,args);
    }
}