package com.wonu.newsun.user.service.impl;

import com.wonu.newsun.user.dao.UserDao;
import com.wonu.newsun.user.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserServiceImplTest
 * @date 2023/11/21 17:06
 * @description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceImplTest {

    @Resource
    private UserService userService;

    @Test
    public void findAll() {
        userService.findAll().forEach(
                user -> System.out.println(user.getUsername())
        );
    }
}