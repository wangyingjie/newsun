package com.wonu.newsun.aop.aspect;

import com.wonu.newsun.common.exception.BmsException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName FlowLimitAspect
 * @date 2023/11/28 18:45
 * @description   利用redis令牌桶算法的切面类
 */
@Aspect
@Component
@Slf4j
public class FlowLimitAspect {

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    /* 定义切点方法以及切点表达式  指定未引入的模块，会编译报错，但只要不单独运行即可，将apo模块引入
    * 要切的模块即可*/
    @Pointcut("execution(* com.wonu.newsun.activity.controller.*Contorller.prays(..))")
    //@annotation(com.wonu.newsun.common.annotation.aop.Test)  切点设置为自定义注解
    public void pointCut(){
    }

    @Before("pointCut()")  //也可以直接使用切点表达式作为参数
    public void preHandler() {

        System.out.println("调用切面方法");
        String luaScript = "return redis.call('CL.THROTTLE',KEYS[1] ,ARGV[1] ,ARGV[2] ,ARGV[3],ARGV[4])";
        List<Long> flag = redisTemplate.execute(
                new DefaultRedisScript<>(luaScript, List.class),
                new ArrayList<String>() {{
                    add("older:limit");
                }},
                99, 10, 100, 5
        );
        log.debug("令牌返回：{}",flag);
        if(flag.get(0) != 0) {
            throw new BmsException(500,"当前祈福人数过多，请稍后重试");
        }
    }
}