package com.wonu.newsun.replication.config;


import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RedissonConfig
 * @date 2023/12/1 12:13
 * @description  Redisson配置类
 */
@Configuration
@Slf4j
public class RedissonConfig {


    @Bean
    public RedissonClient redissonClientConfig() {

        log.debug("==装配replication下的redisson==");
        Config config = new Config();
        config.useSentinelServers()
                .setMasterName("mymaster")
                // use "redis://" for SSL connection  以下两种参数方式都可以
                .addSentinelAddress("redis://192.168.216.129:26379", "redis://192.168.216.129:26381")
                .addSentinelAddress("redis://192.168.216.129:26380");

        return  Redisson.create(config);
    }
}