package com.wonu.newsun.replication.controller;

import com.wonu.newsun.common.result.http.HttpResp;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Random;

/**
 * @author nameless
 * @version 1.0
 * @ClassName SentinelController
 * @date 2023/12/1 11:19
 * @description
 */
@RestController
@RequestMapping("/api/replication")
@Slf4j
public class SentinelController {

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @Resource
    private RedissonClient redissonClient;

    @PutMapping("/doctor")
    public HttpResp DoctorController(String name) {

        RLock myLock = redissonClient.getLock("myLock");
        myLock.lock();
        log.debug("获取分布锁");
        try {
            redisTemplate.opsForValue().set("appointment:" + new Random().nextInt(100)
                    ,"{time: 2023-12-01,name:"+name+"}");
        }finally {
            myLock.unlock();
            log.debug("释放分布锁");
        }

        return HttpResp.success("预约成功");
    }
}