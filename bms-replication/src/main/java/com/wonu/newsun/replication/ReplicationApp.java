package com.wonu.newsun.replication;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ReplicationApp
 * @date 2023/12/1 11:20
 * @description
 */
@SpringBootApplication
@Slf4j
@ComponentScan(basePackages = {"com.wonu.newsun.cache", "com.wonu.newsun.replication"})
public class ReplicationApp {
    public static void main(String[] args) {
        log.debug("版本1.0");
        SpringApplication.run(ReplicationApp.class,args);
    }
}