package com.wonu.newsun.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ShoppingCar
 * @date 2023/11/27 16:41
 * @description  购物车实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShoppingCart {

    private Long userId;
    private List<Goods> goodsList;
    private BigDecimal totalPrice;
}