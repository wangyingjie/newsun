package com.wonu.newsun.domain.entity;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.NoArgsConstructor;

/**
 * (Goods)实体类
 *
 * @author makejava
 * @since 2023-11-22 19:28:13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_goods")
public class Goods {
    
    @TableId(value = "goods_id",type = IdType.AUTO)
    private Long goodsId;
    /**
     * 类型
     */    
    @TableField("type")
    private String type;
    /**
     * 商品名
     */    
    @TableField("goods_name")
    private String goodsName;
    /**
     * 单价
     */    
    @TableField("price")
    private Double price;
    /**
     * 库存 或 数量
     */    
    @TableField("quantity")
    private Integer quantity;
        
    @TableField("create_time")
    private Date createTime;
        
    @TableField("update_time")
    private Date updateTime;
        
    @TableField("create_by")
    private String createBy;

}
