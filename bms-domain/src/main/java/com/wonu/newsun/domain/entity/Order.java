package com.wonu.newsun.domain.entity;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.NoArgsConstructor;

/**
 * (Order)实体类
 *
 * @author makejava
 * @since 2023-11-22 19:28:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_order")
public class Order {
    
    @TableId(value = "order_id",type = IdType.AUTO)
    private Long orderId;
    /**
     * 订单号 ,使用雪花id
     */    
    @TableField("number")
    private Long number;

    @TableField("user_id")
    private Long userId;
    /**
     * 总价
     */    
    @TableField("total_price")
    private BigDecimal totalPrice;
    /**
     * 订单状态 0：正常  -1：数量不足 -2：没货
     */    
    @TableField("status")
    private Integer status;
        
    @TableField("create_time")
    private Date createTime;
        
    @TableField("update_time")
    private Date updateTime;
        
    @TableField("create_by")
    private String createBy;

}
