package com.wonu.newsun.domain.doc;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author nameless
 * @version 1.0
 * @ClassName Book
 * @date 2023/12/5 16:08
 * @description  声明创建的文档为book_index,索引不存在则创建
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "book_index",createIndex = true)
public class BookDoc {

    @Id
    @Field(type = FieldType.Long)
    private Long id;
    @Field(type = FieldType.Text)
    private String title;
    @Field(type = FieldType.Keyword)
    private String author;
    @Field(type = FieldType.Keyword)
    private String ISBN;
    @Field(type = FieldType.Double)
    private BigDecimal price;
    /*存储进es中进行日期格式控制*/
    @Field(type = FieldType.Date,format = DateFormat.year_month_day)
    private Date publication;
    @Field(type = FieldType.Text)
    private String introduction;
    @Field(type = FieldType.Date,format = DateFormat.year_month_day)
    private Date createTime;
}