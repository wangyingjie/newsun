package com.wonu.newsun.domain.entity;

import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Translation)实体类  交易记录
 *
 * @author makejava
 * @since 2023-11-24 14:36:28
 */
@Data
@TableName("t_translation")
public class Translation {
    
    @TableId(value = "translation_id",type = IdType.AUTO)
    private Long transcationId;
        
    @TableField("account_id")
    private Long accountId;
        
    @TableField("order_id")
    private Long orderId;
        
    @TableField("trans_money")
    private Double transMoney;
    /**
     * 交易状态
     */    
    @TableField("status")
    private Integer status;
        
    @TableField("create_time")
    private Date createTime;
        
    @TableField("update_time")
    private Date updateTime;
        
    @TableField("create_by")
    private String createBy;

}
