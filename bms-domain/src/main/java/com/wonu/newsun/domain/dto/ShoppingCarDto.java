package com.wonu.newsun.domain.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ShoppingCarDto
 * @date 2023/11/27 11:54
 * @description  前端选择商品的dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShoppingCarDto {
    private Long goodsId;
    /**
     * 类型
     */
    private String type;
    /**
     * 商品名
     */
    private String goodName;
    /**
     * 单价
     */
    private Double price;
}