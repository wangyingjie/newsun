package com.wonu.newsun.domain.entity;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (OrderItem)实体类
 *
 * @author makejava
 * @since 2023-11-27 17:45:44
 */
@Data
@TableName("t_order_item")
public class OrderItem {
    
    @TableId(value = "order_item_id",type = IdType.AUTO)
    private Long orderItemId;
    /**
     * order表的主键
     */    
    @TableField("order_id")
    private Long orderId;
        
    @TableField("order_number")
    private Long orderNumber;
    /**
     * 商品id
     */    
    @TableField("goods_id")
    private Long goodsId;
    /**
     * 购买数量
     */    
    @TableField("nums")
    private Integer nums;
    /**
     * 订单项总价
     */    
    @TableField("price")
    private BigDecimal price;
        
    @TableField("create_time")
    private Date createTime;
        
    @TableField("update_time")
    private Date updateTime;
        
    @TableField("create_by")
    private String createBy;

}
