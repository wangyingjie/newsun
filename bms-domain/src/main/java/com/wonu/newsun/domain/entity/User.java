package com.wonu.newsun.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author nameless
 * @version 1.0
 * @ClassName User
 * @date 2023/11/21 16:37
 * @description  用户实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_user")
public class User {

    @TableId(value = "user_id")
    private Long userId;
    @TableField("username")
    private String username;
    @TableField("password")
    private String password;
    @TableField("nikename")
    private String nikeName;
    @TableField("role_id")
    private Long roleId;
    @TableField("create_time")
    private Date createTime;
    @TableField("update_time")
    private Date updateTime;
    @TableField("create_by")
    private String createBy;
}