package com.wonu.newsun.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName MessageVo
 * @date 2023/11/22 12:02
 * @description  ws消息体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageVo <T> {
    private String sessionId;
    private T data;
}