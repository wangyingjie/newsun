package com.wonu.newsun.domain.utils;

import java.util.HashMap;

/**
 * @author nameless
 * @version 1.0
 * @ClassName OrderIdMap
 * @date 2023/11/23 21:32
 * @description  自动OrderId生成
 */
public class OrderIdUtils {

    private  static HashMap<String, Character> map = new HashMap();

    public HashMap<String,Character> getMap() {
        return map;
    }

    public static  String getOrderIdByUser(Long userId) {
        String key = String.valueOf(userId);
        //如果key存在，则不改变任何值，返回原值，若key不存在则将 key-1 键值对放入map中，返回null(因为原值为null)
        map.putIfAbsent(key, 'a');
        Character ch = map.get(key);
        map.put(key, (char)(ch + 1));
        return key + "[" + ch + "]";
    }

}