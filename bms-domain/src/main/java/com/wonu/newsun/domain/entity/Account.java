package com.wonu.newsun.domain.entity;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Account)实体类
 *
 * @author makejava
 * @since 2023-11-24 14:22:48
 */
@Data
@TableName("t_account")
public class Account {
    
    @TableId(value = "account_id",type = IdType.AUTO)
    private Long accountId;
        
    @TableField("user_id")
    private Long userId;
    /**
     * 余额
     */    
    @TableField("balance")
    private BigDecimal balance;
        
    @TableField("create_time")
    private Date createTime;
        
    @TableField("update_time")
    private Date updateTime;
        
    @TableField("create_by")
    private String createBy;

}
