package com.wonu.newsun.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RegisterDto
 * @date 2023/11/21 19:55
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterDto {

    @NotBlank(message = "用户名不能为空")
    private String username;
    private String nikeName;
    @NotBlank(message = "密码不能为空")
    private String password;
    private String rePass;
}