package com.wonu.newsun.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NegativeOrZero;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author nameless
 * @version 1.0
 * @ClassName Book
 * @date 2023/12/5 16:08
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_book")
public class Book {
    @TableId(value = "book_id", type = IdType.AUTO)
    private Long id;
    @TableField("book_title")
    private String title;
    @TableField("book_author")
    private String author;
    @TableField("book_isbn")
    private String ISBN;
    @TableField("book_price")
    private BigDecimal price;
    @TableField("book_publication")
    private Date publication;
    @TableField("book_introduction")
    private String introduction;
    @TableField("book_create_time")
    private Date createTime;
}