package com.wonu.newsun.activity.config;

import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RedissonConfig
 * @date 2023/11/30 12:06
 * @description
 */
@Configuration
@Slf4j
public class RedissonConfig {

    @Bean
    public RedissonClient redissonClient() {
        log.debug("==Redisson配置==");
        Config config = new Config();
        //设置redisson的配置   配置是单个redisson, redis的服务器地址
        config.useSingleServer().setAddress("redis://192.168.216.129:6390");
        return Redisson.create(config);
    }
}