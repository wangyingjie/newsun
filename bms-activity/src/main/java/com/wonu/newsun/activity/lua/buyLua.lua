local number = redis.call('get', KEYS[1])
local num = tonumber(number)
if num > 0 then
    return redis.call("decr", KEYS[1])
else
    return 0
end