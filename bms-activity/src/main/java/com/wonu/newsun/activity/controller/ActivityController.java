package com.wonu.newsun.activity.controller;

import com.wonu.newsun.common.result.http.HttpResp;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;


/**
 * @author nameless
 * @version 1.0
 * @ClassName ActivityController
 * @date 2023/11/28 14:39
 * @description
 */
@RestController
@RequestMapping("/api/activity")
@Slf4j
public class ActivityController {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private RedissonClient redissonClient;


    @CrossOrigin
    @GetMapping("/prays")
    public HttpResp prays() {
        //String script = "return redis.call('CL.THROTTLE',KEYS[1], ARGV[1], ARGV[2], ARGV[3], ARGV[4])";
        return HttpResp.success("恭喜祈福成功");

    }


    @CrossOrigin
    @PutMapping("/redisson")
    public HttpResp redisson() {

        RLock myLock = redissonClient.getLock("myLock");
        myLock.lock();
        log.debug("获取了锁");
        try {
            Integer number = (Integer) redisTemplate.opsForValue().get("apple");
            if (number > 0) {
                number--;
                redisTemplate.opsForValue().set("apple", number);
                log.info("当前剩余数量：{}", number);

                return HttpResp.success("抢购成功，还有" + number + "个");
            } else {
                log.error("已抢完");
                return HttpResp.fail(500, "已抢完");
            }
        } finally {
            myLock.unlock();
            log.debug("释放了锁");
        }
}


    @CrossOrigin
    @PutMapping("/buy")
    public synchronized HttpResp buy() {

        Integer number = (Integer) redisTemplate.opsForValue().get("apple");

        if (number > 0) {

            redisTemplate.opsForValue().decrement("apple");
            Integer apple = (Integer) redisTemplate.opsForValue().get("apple");
            log.info("当前剩余数量：{}", apple);
            return HttpResp.success("购买成功，还有" + apple + "个");
        } else {
            log.error("已抢完");
            return HttpResp.fail(500, "已抢完");
        }

    }


    @CrossOrigin
    @PutMapping("/buyLua")
    public synchronized HttpResp buyLua() {

        DefaultRedisScript<Long> redisScript = new DefaultRedisScript<Long>();
        redisScript.setResultType(Long.class);
        redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("lua/buyLua.lua")));
        Long execute = redisTemplate.execute(redisScript, new ArrayList<String>() {{
            add("apple");
        }});
        if (execute > 0) {
            log.info("当前剩余数量：{}", execute);
            return HttpResp.success("购买成功，还有" + execute + "个");
        } else {
            log.error("已抢完");
            return HttpResp.fail(500, "已抢完");
        }
    }


    /**
     * @param
     * @return HttpResp
     * @author nameless
     * @date 2023/11/29 17:52
     * @description 简单的分布式锁逻辑
     */
    @PutMapping("/redisLock")
    public HttpResp lockeTest() {

        // 对应 setnx命令，设置成功返回true；设置失败(已存在)返回false
        Boolean ifAbsent = redisTemplate.opsForValue().setIfAbsent("lock", "ok");

        if (ifAbsent) {  //获取到锁
            log.debug("已获取锁");
            try {
                Integer number = (Integer) redisTemplate.opsForValue().get("apple");
                if (number > 0) {
                    number--;
                    redisTemplate.opsForValue().set("apple", number);
                    log.info("当前剩余数量：{}", number);

                    return HttpResp.success("抢购成功，还有" + number + "个");
                } else {
                    log.error("已抢完");
                    return HttpResp.fail(500, "已抢完");
                }
            } finally {
                redisTemplate.delete("lock");
                log.debug("释放了锁");
            }
        } else {
            log.debug("未获取锁，无法抢购");
            return HttpResp.fail(500, "未获取锁，无法抢购");
        }

    }
}