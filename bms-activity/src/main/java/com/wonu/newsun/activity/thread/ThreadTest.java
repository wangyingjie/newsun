package com.wonu.newsun.activity.thread;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ThreadTest
 * @date 2023/11/30 11:24
 * @description
 */
@Slf4j
public class ThreadTest {

    private  static final ReentrantLock lock =new ReentrantLock();
    private static int val = 100;

    public static void main(String[] args) {

        for (int i = 0; i < 3; i++) {
            new Thread(()->{
                while (val > 0){
                    lock.lock();
                    log.debug("上锁");
                    try{
                        if(val > 0) {
                            System.out.println("线程："+ Thread.currentThread().getName() +"成功执行业务,剩余："+ --val);
                        }
                    }catch (Exception e){
                        System.out.println(e.getMessage());
                    }finally {
                        lock.unlock();
                        log.debug("释放锁");
                    }
                }
            }).start();
        }
    }
}