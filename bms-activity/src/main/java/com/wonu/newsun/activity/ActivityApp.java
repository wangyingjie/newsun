package com.wonu.newsun.activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ActivityApp
 * @date 2023/11/28 11:58
 * @description
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wonu.newsun"})
public class ActivityApp {
    public static void main(String[] args) {
        SpringApplication.run(ActivityApp.class,args);
    }
}