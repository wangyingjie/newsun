package com.wonu.newsun.poi;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author nameless
 * @version 1.0
 * @ClassName PoiUtils
 * @date 2023/11/24 16:43
 * @description   【未完成】
 */
public class PoiUtils {

    /**
     * @author nameless
     * @date 2023/11/25 14:38
     * @param descFilePath
     * @return void
     * @description 使用poi将数据写入到excel[未完成]
     */
    public static void writeToExcel(String descFilePath) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        workbook.createSheet("用户信息表");

    }


    /**
     * @author nameless
     * @date 2023/11/25 14:38
     * @param sourceFilePath
     * @return void
     * @description 读取excel文件写入到内存
     */
    public static void readFromExcel(String sourceFilePath) throws IOException, InvalidFormatException {


        File file = new File(sourceFilePath);


        FileInputStream is = new FileInputStream(file);
        XSSFWorkbook workbook = new XSSFWorkbook(is);



        XSSFSheet sheet = workbook.getSheet("用户信息");
        int rowNum = sheet.getLastRowNum();   // rowNum 每个表单最后一行表记录 包含当前数 <=
        for (int i = 0; i <= rowNum; i++) {
            XSSFRow row = sheet.getRow(i);
            short cellNum = row.getLastCellNum();  //cellNum 每一行的最后列数 行数据不包含当前数 <
            for (int j = 0; j < cellNum; j++) {
                XSSFCell cell = row.getCell(j);
                CellType cellType = cell.getCellType();
                System.out.print(cell + "\t");
                switch (cellType) {
                    case NUMERIC:
                        break;
                    case STRING:
                        break;
                    case BLANK:
                        break;
                    default:
                        break;
                }
            }
            System.out.println("");
        }
    }
}