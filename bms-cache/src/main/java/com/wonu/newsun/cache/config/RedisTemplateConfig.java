package com.wonu.newsun.cache.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.Date;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RedisTemplateConfig
 * @date 2023/11/22 18:46
 * @description  redisTemplate 配置类
 */
@Configuration
@Slf4j
public class RedisTemplateConfig {
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {

        // 创建一个 RedisTemplate
        RedisTemplate<String, Object> redisTemplate =
                new RedisTemplate<>();
        //调用setConnectionFactory（继承自RedisAccess类）将redisConnectionFactory放入模板
        redisTemplate.setConnectionFactory(redisConnectionFactory);

        // 通用的 key- value 的序列化
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());

        // hash类型 hashkey hashvalue 序列化
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());

        log.debug("==装配自定义序列化的redisTemplate==");

        return redisTemplate;
    }
}