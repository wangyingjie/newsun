package com.wonu.newsun.pay.service;

import com.wonu.newsun.domain.entity.Account;

import java.math.BigDecimal;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AccountService
 * @date 2023/11/24 14:25
 * @description
 */
public interface AccountService {

    Account findByUserId(Long id);

    void modifyBalance(Account account);
}
