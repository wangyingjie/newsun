package com.wonu.newsun.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author nameless
 * @version 1.0
 * @ClassName PayApp
 * @date 2023/11/24 10:44
 * @description
 */
@SpringBootApplication
public class PayApp {
    public static void main(String[] args) {
        SpringApplication.run(PayApp.class,args);
    }
}