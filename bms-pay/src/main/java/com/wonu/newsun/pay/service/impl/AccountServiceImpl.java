package com.wonu.newsun.pay.service.impl;

import com.wonu.newsun.domain.entity.Account;
import com.wonu.newsun.pay.dao.AccountDao;
import com.wonu.newsun.pay.service.AccountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AccountServiceImpl
 * @date 2023/11/24 14:26
 * @description
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Resource
    private AccountDao accountDao;

    @Override
    public Account findByUserId(Long id) {

        return accountDao.selectById(id);
    }


    @Override
    public void modifyBalance(Account account) {
        Account account1 = accountDao.selectById(account.getAccountId());
        account1.setBalance(account.getBalance());
        accountDao.updateById(account1);
    }
}