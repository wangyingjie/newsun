package com.wonu.newsun.pay.controller;

import com.wonu.newsun.common.result.http.HttpResp;
import com.wonu.newsun.domain.entity.Account;
import com.wonu.newsun.pay.service.AccountService;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @author nameless
 * @version 1.0
 * @ClassName PayController
 * @date 2023/11/24 10:44
 * @description  支付模块api
 */
@RestController
@RequestMapping("/api/pay")
public class PayController {

    @Resource
    private AccountService accountService;

    /**
     * @author nameless
     * @date 2023/11/24 10:49
     * @param
     * @return HttpResp
     * @description 查询订单中数量，减去账户余额
     *  uri 需要 总价和 userid
     */
    @GetMapping("/payOrder")
    public HttpResp payForOrder(BigDecimal totalPrice, Long userId) {

        //查询账户
        Account account = accountService.findByUserId(userId);

        BigDecimal balance = account.getBalance();
        BigDecimal subtract = balance.subtract(totalPrice);
        BigDecimal zero = new BigDecimal(0.00);
        if(subtract.compareTo(zero) >= 1) {
            //余额充足， 扣款成功，返回支付成功


            return new HttpResp(200, "支付成功",null);
        }
        return new HttpResp(500, "余额不足支付失败",null);

    }
}