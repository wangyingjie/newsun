package com.wonu.newsun.pay.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.newsun.domain.entity.Account;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AccountDao
 * @date 2023/11/24 14:24
 * @description
 */
@Mapper
public interface AccountDao extends BaseMapper<Account> {
}
